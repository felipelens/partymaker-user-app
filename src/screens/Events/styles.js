import styled from "styled-components/native";
import { Card, Appbar } from "react-native-paper";
import { BlurView } from "expo-blur";

export const AppbarHeader = styled(Appbar.Header)`
  background-color: transparent;
  box-shadow: none;
`;

export const TopText = styled.Text`
  color: #fff;
  font-size: 12px;
  text-align: center;
`;

export const StyledCard = styled(Card)`
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  border-radius: 6px;
  overflow: hidden;
  margin-top: 20px;
`;

export const CardTitle = styled(BlurView)`
  flex: 1;
  flex-direction: row;
  align-items: center;
  background: rgba(69, 65, 114, 0.5);
  align-items: flex-start;
  justify-content: space-between;
  flex-basis: 60px;
  position: absolute;
  bottom: 0;
  left: 0;
`;

export const TitleView = styled.View`
  flex-basis: 80%;
  padding: 9px 10px 8px 0;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

export const TitleText = styled.Text`
  font-size: 15px;
  color: #fff;
  flex-shrink: 1;
`;

export const DateView = styled.View`
  flex: 1;
  flex-direction: column;
  width: 50px;
  height: 60px;
  padding: 5px;
  justify-content: center;
  align-items: center;
`;

export const Day = styled.Text`
  text-align: center;
  line-height: 22px;
  font-size: 26px;
  color: #a37ff1;
`;

export const Month = styled.Text`
  text-align: center;
  color: #a37ff1;
`;
