import React from "react";
import PropTypes from "prop-types";
import { View, ScrollView } from "react-native";
import { Appbar, Card } from "react-native-paper";
import { LinearGradient } from "expo-linear-gradient";
import {
  TopText,
  StyledCard,
  CardTitle,
  TitleView,
  TitleText,
  DateView,
  Day,
  Month,
  AppbarHeader
} from "./styles";

const Events = ({ navigation }) => (
  <View style={{ flex: 1 }}>
    <LinearGradient colors={["#2f2c54", "#1f1e2f"]} style={{ flex: 1 }}>
      <AppbarHeader>
        <Appbar.Action
          icon="menu"
          color="#ffffff"
          onPress={navigation.openDrawer}
        />
        <Appbar.Content
          title="Events"
          color="#ffffff"
          style={{ alignItems: "center" }}
        />
        <Appbar.Action icon="magnify" color="#ffffff" />
        <Appbar.Action icon="filter-variant" color="#ffffff" />
      </AppbarHeader>
      <ScrollView style={{ padding: 20 }}>
        <TopText>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit nulla am magna
          tincidunt ultrices orci tortor habitant in.
        </TopText>
        <StyledCard>
          <Card.Cover
            source={{ uri: "https://picsum.photos/700" }}
            style={{ height: 220 }}
          />
          <CardTitle>
            <DateView>
              <Day>28</Day>
              <Month>MAR</Month>
            </DateView>
            <TitleView>
              <TitleText numberOfLines={2}>
                Party name presents: Meu bichu é uma peça 3
              </TitleText>
            </TitleView>
          </CardTitle>
        </StyledCard>
        <StyledCard>
          <Card.Cover
            source={{ uri: "https://picsum.photos/700" }}
            style={{ height: 220 }}
          />
          <CardTitle>
            <DateView>
              <Day>28</Day>
              <Month>MAR</Month>
            </DateView>
            <TitleView>
              <TitleText numberOfLines={2}>
                Party name presents: Meu bichu é uma peça 3
              </TitleText>
            </TitleView>
          </CardTitle>
        </StyledCard>
        <StyledCard>
          <Card.Cover
            source={{ uri: "https://picsum.photos/700" }}
            style={{ height: 220 }}
          />
          <CardTitle>
            <DateView>
              <Day>28</Day>
              <Month>MAR</Month>
            </DateView>
            <TitleView>
              <TitleText numberOfLines={2}>
                Party name presents: Meu bichu é uma peça 3
              </TitleText>
            </TitleView>
          </CardTitle>
        </StyledCard>
      </ScrollView>
    </LinearGradient>
  </View>
);

Events.propTypes = {
  navigation: PropTypes.shape
};

export default Events;
