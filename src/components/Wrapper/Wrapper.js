import "react-native-gesture-handler";
import React, { useState, useEffect } from "react";
import {
  configureFonts,
  DarkTheme,
  Provider as PaperProvider
} from "react-native-paper";
import * as Font from "expo-font";
import Navigation from "../Navigation";

const fontConfig = {
  default: {
    regular: {
      fontFamily: "Lato, sans-serif",
      fontWeight: "normal"
    },
    light: {
      fontFamily: "Lato, sans-serif",
      fontWeight: "normal"
    },
    thin: {
      fontFamily: "Lato, sans-serif",
      fontWeight: "normal"
    }
  }
};

fontConfig.ios = fontConfig.default;
fontConfig.android = fontConfig.default;
fontConfig.web = fontConfig.default;

const theme = {
  ...DarkTheme,
  fonts: configureFonts(fontConfig)
};

const Wrapper = () => {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  const loadFonts = async () => {
    await Font.loadAsync({
      Lato: require("../../../assets/fonts/Lato-Regular.ttf")
    });
    setFontsLoaded(true);
  };

  useEffect(() => {
    loadFonts();
  }, []);

  if (!fontsLoaded) return null;

  return (
    <PaperProvider theme={theme}>
      <Navigation />
    </PaperProvider>
  );
};

export default Wrapper;
