import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import { Events, Wallet } from "../../screens";

const Drawer = createDrawerNavigator();

const Navigation = () => (
  <NavigationContainer>
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Eventos" component={Events} />
      <Drawer.Screen name="Carteira" component={Wallet} />
    </Drawer.Navigator>
  </NavigationContainer>
);

export default Navigation;
